package com.example.todolist.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.todolist.R
import com.example.todolist.adapter.RecyclerAdapter
import com.example.todolist.model.ToDoItem
import com.example.todolist.viewmodels.MainViewModel
import com.example.todolist.databinding.ActivityMainBinding
import com.google.android.material.floatingactionbutton.FloatingActionButton
import android.content.Intent


class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel
    private lateinit var recyclerAdapter: RecyclerAdapter
    lateinit var mRecycler: RecyclerView
    lateinit var binder : ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binder = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = MainViewModel.create(this)
        mRecycler = binder.recyclerVw
        val linearLayoutManager = LinearLayoutManager(this)
        mRecycler.layoutManager = linearLayoutManager
        val entityViewModels = viewModel.cards.value!!
        recyclerAdapter = RecyclerAdapter(entityViewModels)

        mRecycler.adapter = recyclerAdapter

        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener{
            val intent = Intent(this, AdderActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
            startActivity(intent)
        }
        viewModel.cards.observe(this,
            Observer<List<ToDoItem>> { todoList ->
                mRecycler.adapter!!.notifyDataSetChanged()
            })
    }
}
