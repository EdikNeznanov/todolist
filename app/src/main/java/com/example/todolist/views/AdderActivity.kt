package com.example.todolist.views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import com.example.todolist.R
import com.example.todolist.databinding.ActivityAdderBinding
import com.example.todolist.model.ToDoItem
import com.example.todolist.viewmodels.MainViewModel

class AdderActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel
    lateinit var binder : ActivityAdderBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binder = DataBindingUtil.setContentView(this, R.layout.activity_adder)
        viewModel = MainViewModel.create(this)
        val button = binder.addRec
        button.setOnClickListener {
            val editTextName  = binder.editName
            val editTextDesc = binder.editDesc
            viewModel.setValues(ToDoItem(editTextName.text.toString(), editTextDesc.text.toString()))
           val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
        }
    }
}
