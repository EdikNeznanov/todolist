package com.example.todolist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.todolist.R
import com.example.todolist.model.ToDoItem
import androidx.databinding.DataBindingUtil
import com.example.todolist.databinding.ListItemBinding


class RecyclerAdapter(
    private val list: ArrayList<ToDoItem>
) : RecyclerView.Adapter<RecyclerAdapter.ToDoViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ToDoViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: ListItemBinding = DataBindingUtil.inflate(inflater, R.layout.list_item, parent,false)
        return ToDoViewHolder(binding)
    }


    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ToDoViewHolder, position: Int) {
        val item: ToDoItem = list[position]
        holder.bind(item)
    }

    class ToDoViewHolder(private var binding: ListItemBinding): RecyclerView.ViewHolder(binding.root){

        fun bind(item: ToDoItem) {
            binding.todo = item
            binding.executePendingBindings()
        }
    }
}
