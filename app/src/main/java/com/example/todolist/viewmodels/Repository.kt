package com.example.todolist.viewmodels

import androidx.lifecycle.MutableLiveData
import com.example.todolist.model.ToDoItem


object Repository {
    private var dataSet = ArrayList<ToDoItem>()

    var data = MutableLiveData<ArrayList<ToDoItem>>()
    private set

            init {
                data.value = dataSet
            }

    fun addEntry(entry: ToDoItem) {
        dataSet.add(entry)
        data.value = dataSet
    }
}