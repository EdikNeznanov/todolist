package com.example.todolist.model

data class ToDoItem(val name: String, val description: String)
//data class for cards
