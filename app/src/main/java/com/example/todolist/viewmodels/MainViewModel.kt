package com.example.todolist.viewmodels

import androidx.lifecycle.*
import com.example.todolist.model.ToDoItem
import com.example.todolist.views.AdderActivity
import com.example.todolist.views.MainActivity

class MainViewModel: ViewModel() {

    var cards: LiveData<ArrayList<ToDoItem>> =  Repository.data


    var listofEntries = ArrayList<ToDoItem>()

    companion object{
        fun create(activity: MainActivity): MainViewModel{
            return ViewModelProviders.of(activity).get(MainViewModel::class.java)
        }
        fun create(activity: AdderActivity): MainViewModel{
            return ViewModelProviders.of(activity).get(MainViewModel::class.java)
        }
    }
    fun setValues(values: ToDoItem){
        Repository.addEntry(values)
        cards = Repository.data
//        listofEntries.add(values)
//        cards = MutableLiveData<ArrayList<ToDoItem>>().apply {
//            value = listofEntries
//        }
    }
}
